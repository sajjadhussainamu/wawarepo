<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<head>
<title>Sign up</title>
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	rel="stylesheet" id="bootstrap-css">
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.0.0/css/font-awesome.min.css">
</head>
<body>
	<div class="container">

		<form:form id="tCredential" name="tCredential"
			action="transactionProcess" method="post"
			modelAttribute="tCredential" autocomplete="off">

			<div class="form-group text-danger text-center" >
			${msg}</div>
			<div class="form-group text-success text-center" >
			${successMsg}</div>
                      

			<div class="form-group">
				<label>Transaction Amount</label> <input id="transactionAmount"
					name="transactionAmount" maxlength="45" cssClass="form-control" />
			</div>

			<div class="form-group">
				<label>Transaction Id</label>
				<form:input path="transactionUser" id="transactionUser"
					name="transactionUser" maxlength="45" cssClass="form-control" />
			</div>
			<div class="form-group">
				<label>Transaction Password</label>
				<form:input path="transactionPassword" id="transactionPassword"
					name="transactionPassword" type="password" maxlength="45" cssClass="form-control" />
			</div>

			<button type="submit" class="btn btn-lg btn-success btn-block"
				value="Details">Go</button>
		</form:form>
	</div>
</body>
</html>