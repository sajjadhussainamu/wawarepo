<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<head>
<title>Registration</title>
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	rel="stylesheet" id="bootstrap-css">
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.0.0/css/font-awesome.min.css">
</head>
<body>
	<div class="container">

		<form:form id="userPage" name="userPage" action="registrationProcess"
			method="post" modelAttribute="user" autocomplete="off">

			<div class="form-group"></div>
			<div class="form-group text-success">${successmsg}</div>
			<div class="form-group text-danger">${errrMsg}</div>

			<%-- <label>User ID</label>
				<form:input path="id" id="id" name="id" maxlength="32"
					cssClass="form-control" /> --%>



			<div class="form-group">
				<label>First Name</label>
				<form:input path="firstName" id="firstName" name="firstName"
					maxlength="45" cssClass="form-control" />
			</div>
			<div class="form-group">
				<label>Last Name</label>
				<form:input path="lastName" id="lastName" name="lastName"
					maxlength="45" cssClass="form-control" />
			</div>
			<div class="form-group">
				<label>Role</label>
				<form:select path="role" id="role" name="role" maxlength="45"
					items="${roles}" cssClass="form-control" />
			</div>
			<div class="form-group">
				<label>Active</label>
				<form:select path="active" id="active" name="active" maxlength="45"
					items="${active}" cssClass="form-control" />
			</div>

			<div class="form-group">
				<label>User Name</label>
				<form:input path="userName" id="userName" name="userName"
					maxlength="45" cssClass="form-control" />
			</div>

			<div class="form-group">
				<label>Password</label>
				<form:input path="password" id="password" name="password"
					type="password" maxlength="45" cssClass="form-control" />
			</div>

			<button type="submit" class="btn btn-lg btn-success btn-block"
				value="Details">Save</button>
		</form:form>
	</div>
</body>
</html>