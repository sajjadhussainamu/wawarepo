package hcl.training.wawascreening;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import hcl.training.wawascreening.modles.UserTransactionCredential;
import hcl.training.wawascreening.repository.UserRepository;

@SpringBootApplication
@EnableJpaRepositories(basePackageClasses = { UserRepository.class, UserTransactionCredential.class })
public class WawaScreeningApplication {

	public static void main(String[] args) {
		SpringApplication.run(WawaScreeningApplication.class, args);
	}

}
