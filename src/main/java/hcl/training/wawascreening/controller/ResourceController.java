package hcl.training.wawascreening.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import hcl.training.wawascreening.modles.Customer;
import hcl.training.wawascreening.modles.Transaction;
import hcl.training.wawascreening.modles.User;
import hcl.training.wawascreening.modles.UserTransactionCredential;
import hcl.training.wawascreening.service.CustomerService;
import hcl.training.wawascreening.service.TransactionService;
import hcl.training.wawascreening.service.UserService;
import hcl.training.wawascreening.service.UserTransactionCredentialService;

@Controller
public class ResourceController {

	@Autowired
	private UserService userService;

	@Autowired
	private UserTransactionCredentialService userTransCredentialService;

	@Autowired
	private CustomerService customerService;

	@Autowired
	private TransactionService transactionService;

	@Autowired
	ObjectFactory<HttpSession> httpSessionFactory;

	@GetMapping("/registration")
	public ModelAndView registration(ModelMap map, User user) {
		this.registrationUtility(map, user);
		return new ModelAndView("registration", map);
	}

	@PostMapping("/registrationProcess")
	public ModelAndView registrationProcess(@ModelAttribute("user") User user, ModelMap map) {

		Optional<User> userAval = userService.getByUserName(user.getUserName());

		if (!userAval.isPresent()) {
			userService.createUser(user);
			map.addAttribute("successmsg", "Successfully Registration done ");
		} else {
			map.addAttribute("errrMsg", "User alredy exist try different one ");
		}
		this.registrationUtility(map, user);
		return new ModelAndView("registration", map);
	}

	@GetMapping("/transactionForm")
	public ModelAndView transactionForm(ModelMap map, UserTransactionCredential tCredential) {

		tCredential.setTransactionUser(null);
		tCredential.setTransactionPassword(null);

		map.put("tCredential", tCredential);
		return new ModelAndView("transactionForm", map);
	}

	@PostMapping("/transactionProcess")
	public ModelAndView transactionProcess(@ModelAttribute UserTransactionCredential tCredential,
			@RequestParam("transactionAmount") Double transactionAmount, ModelMap map) {

		Optional<UserTransactionCredential> trancCredential = userTransCredentialService
				.getByTransactionUserAndTransactionPassword(tCredential.getTransactionUser(),
						tCredential.getTransactionPassword());

		HttpSession session = httpSessionFactory.getObject();
		Optional<User> uOptional = (Optional<User>) session.getAttribute("user");
		
		UserTransactionCredential userTranCredential = userTransCredentialService
				.getByTransactionUser(tCredential.getTransactionUser());
		
		if (userTranCredential != null && userTranCredential.getFroudCount() >= 3) {
			map.put("msg", "More time third time wrong password Transaction is temporary Blocked");
			return this.transactionForm(map, tCredential);
		}
		if (!trancCredential.isPresent() || uOptional.get().getId() != trancCredential.get().getUser().getId()) {

			userTranCredential.setFroudCount(userTranCredential.getFroudCount() + 1);

			userTransCredentialService.updateUserTransactionCredential(userTranCredential);
			map.put("msg", "user Transaction or password does not exist");
			return this.transactionForm(map, tCredential);
		}
		synchronized (this) {
			Customer customer = customerService.getCustomerByUserId(uOptional.get());
			if (transactionAmount > customer.getBalance()) {
				map.put("msg", "Amount is insufficient");
				return this.transactionForm(map, tCredential);
			}
			customer.setBalance(customer.getBalance() - transactionAmount);
			customerService.updateCustomerBalance(customer);
			Transaction tranDetail = this.setTransationDetails(customer, transactionAmount);
			transactionService.insertTransaction(tranDetail);
		}

		map.put("successMsg", "Transaction Success");
		return this.transactionForm(map, tCredential);
	}

	@GetMapping("/user")
	public String user() {
		return "welcome";
	}

	@GetMapping("/admin")
	public String admin() {
		return "welcome";
	}

	@GetMapping("/")
	@ResponseBody
	public String welcome() {

		return "default";

	}

	private void registrationUtility(ModelMap map, User user) {
		map.put("user", user);

		Map<String, String> roleMap = new HashMap<>();
		roleMap.put("ROLE_ADMIN", "ADMIN");
		roleMap.put("ROLE_USER", "USER");
		map.addAttribute("roles", roleMap);

		Map<String, String> active = new HashMap<String, String>();
		active.put("true", "TRUE");
		active.put("false", "FALSE");
		map.addAttribute("active", active);

	}

	private Transaction setTransationDetails(Customer customer, Double transactionAmount) {
		Transaction tran = new Transaction();
		tran.setAccountNumber(customer.getAccountNumber());
		tran.setAmount(transactionAmount);
		tran.setTransactionType("DR");
		return tran;
	}
}
