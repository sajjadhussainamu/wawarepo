package hcl.training.wawascreening.service;

import java.util.Optional;

import hcl.training.wawascreening.modles.UserTransactionCredential;

public interface UserTransactionCredentialService {

	public void updateUserTransactionCredential(UserTransactionCredential userTransactionCredential);
	
	public UserTransactionCredential getByTransactionUser(String transactionUser);

	public  Optional<UserTransactionCredential> getByTransactionUserAndTransactionPassword(String transactionUser,
			String transactionPassword);
}
