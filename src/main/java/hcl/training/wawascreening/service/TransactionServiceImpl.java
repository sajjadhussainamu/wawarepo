package hcl.training.wawascreening.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hcl.training.wawascreening.modles.Transaction;
import hcl.training.wawascreening.repository.TransactionRepository;

@Service
public class TransactionServiceImpl implements TransactionService {

	@Autowired
	private TransactionRepository transactionRepository;

	@Override
	public void insertTransaction(Transaction transaction) {
		transactionRepository.save(transaction);
	}

}
