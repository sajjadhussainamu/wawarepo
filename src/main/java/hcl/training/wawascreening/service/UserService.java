package hcl.training.wawascreening.service;

import java.util.Optional;

import hcl.training.wawascreening.modles.User;

public interface UserService {

	Optional<User> getByUserName(String username);

	void createUser(User user);
}
