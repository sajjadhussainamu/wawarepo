package hcl.training.wawascreening.service;

import hcl.training.wawascreening.modles.Customer;
import hcl.training.wawascreening.modles.User;

public interface CustomerService {

	
	public Customer getCustomerByUserId(User usreId);
	
	public void updateCustomerBalance(Customer customer);
}
