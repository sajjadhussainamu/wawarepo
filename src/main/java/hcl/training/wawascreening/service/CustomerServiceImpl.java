package hcl.training.wawascreening.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hcl.training.wawascreening.modles.Customer;
import hcl.training.wawascreening.modles.User;
import hcl.training.wawascreening.repository.CustomerRepository;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerRepository customerRepository;

	@Override
	public Customer getCustomerByUserId(User user) {
		Optional<Customer> customer = customerRepository.findByUser(user);

		System.out.println("customer " + customer.get());
		return customer.get();
	}

	@Override
	public void updateCustomerBalance(Customer customer) {
		customerRepository.save(customer);
	}

}
