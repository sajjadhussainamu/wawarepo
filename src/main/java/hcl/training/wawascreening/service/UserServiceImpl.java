package hcl.training.wawascreening.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hcl.training.wawascreening.modles.User;
import hcl.training.wawascreening.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public Optional<User> getByUserName(String username) {
		return userRepository.findByUserName(username);
	}

	@Override
	public void createUser(User user) {
		userRepository.save(user);
	}

}
