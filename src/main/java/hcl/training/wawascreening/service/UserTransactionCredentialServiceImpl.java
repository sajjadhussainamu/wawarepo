package hcl.training.wawascreening.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hcl.training.wawascreening.modles.UserTransactionCredential;
import hcl.training.wawascreening.repository.UserTransactionCredentialRepository;

@Service
public class UserTransactionCredentialServiceImpl implements UserTransactionCredentialService {

	@Autowired
	private UserTransactionCredentialRepository userTransactionCredentialRepository;

	@Override
	public void updateUserTransactionCredential(UserTransactionCredential userTransactionCredential) {
		userTransactionCredentialRepository.save(userTransactionCredential);

	}

	@Override
	public Optional<UserTransactionCredential> getByTransactionUserAndTransactionPassword(String transactionUser,
			String transactionPassword) {
		return userTransactionCredentialRepository.findByTransactionUserAndTransactionPassword(transactionUser,
				transactionPassword);
	}

	@Override
	public UserTransactionCredential getByTransactionUser(String transactionUser) {
		return userTransactionCredentialRepository.findByTransactionUser(transactionUser);
	}

}
