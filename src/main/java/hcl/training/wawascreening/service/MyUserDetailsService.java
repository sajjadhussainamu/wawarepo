package hcl.training.wawascreening.service;

import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import hcl.training.wawascreening.MyUserDetails;
import hcl.training.wawascreening.modles.User;
import hcl.training.wawascreening.repository.UserRepository;

@Service
public class MyUserDetailsService implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	ObjectFactory<HttpSession> httpSessionFactory;

	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<User> user = userRepository.findByUserName(username);

		user.orElseThrow(() -> new UsernameNotFoundException("Not Found " + username));
		
		HttpSession session = httpSessionFactory.getObject();
		
		session.setAttribute("user", user);

		return user.map(MyUserDetails::new).get();
	}

}
