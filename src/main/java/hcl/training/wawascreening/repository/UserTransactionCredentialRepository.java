package hcl.training.wawascreening.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hcl.training.wawascreening.modles.UserTransactionCredential;

@Repository
public interface UserTransactionCredentialRepository extends JpaRepository<UserTransactionCredential, Integer> {

	
	UserTransactionCredential findByTransactionUser(String transactionUser);

	Optional<UserTransactionCredential> findByTransactionUserAndTransactionPassword(String transactionUser,
			String transactionPassword);
}
