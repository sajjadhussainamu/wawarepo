package hcl.training.wawascreening.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hcl.training.wawascreening.modles.Transaction;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Integer> {

}
