package hcl.training.wawascreening.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hcl.training.wawascreening.modles.Customer;
import hcl.training.wawascreening.modles.User;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer> {

	Optional<Customer> findByUser(User user);

}
