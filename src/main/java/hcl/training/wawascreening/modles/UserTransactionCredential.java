package hcl.training.wawascreening.modles;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Table
@Entity
public class UserTransactionCredential {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column(name = "Trans_User")
	private String transactionUser;

	@Column(name = "Trans_Pass")
	private String transactionPassword;

	@Column(name = "Trans_Active")
	private boolean active;

	@OneToOne
	private User user;

	@Column(name = "froud_count")
	private int froudCount = 0;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTransactionUser() {
		return transactionUser;
	}

	public void setTransactionUser(String transactionUser) {
		this.transactionUser = transactionUser;
	}

	public String getTransactionPassword() {
		return transactionPassword;
	}

	public void setTransactionPassword(String transactionPassword) {
		this.transactionPassword = transactionPassword;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public int getFroudCount() {
		return froudCount;
	}

	public void setFroudCount(int froudCount) {
		this.froudCount = froudCount;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "UserTransactionCredential [id=" + id + ", transactionUser=" + transactionUser + ", transactionPassword="
				+ transactionPassword + ", active=" + active + ", user=" + user + ", froudCount=" + froudCount + "]";
	}

}
